// jars dimentions
const jars = {
  t7: 46,
  // 't7m': 58,
  t5: 64,
  // 't5m': 67,
  t4: 77,
  // 't4m': 81,
  // 't4mm': 87,
  t3: 99,
};

const triangleWidth = 420;

// Difference between hole radius and jar radius
const holePlay = 1;

// jars pattern
const t3 = [jars.t3, jars.t3, jars.t3];
const t4 = [jars.t4, jars.t4, jars.t4, jars.t4];
const t5 = [jars.t5, jars.t5, jars.t5, jars.t5, jars.t5];
const t7 = [jars.t7, jars.t7, jars.t7, jars.t7, jars.t7, jars.t7, jars.t7];

// const t3 = [jars.t3];
// const t4 = [jars.t4];
// const t5 = [jars.t5];
// const t7 = [jars.t7];

const triangles = [[t3, t4], [t5, t7]];

// Display construction lines or not
let isConstr = false;

function drawCircle(ctx, centerX, centerY, d, construction = false) {
  ctx.beginPath();
  ctx.arc(centerX, centerY, 0.5 * d, 0, 2 * Math.PI, false);
  if (construction) {
    ctx.strokeStyle = 'black';
    ctx.lineWidth = 1;
    ctx.stroke();
  } else {
    ctx.fillStyle = 'lightgray';
    ctx.fill();
  }
}

// const saveData = (function saveData() {
//   const a = document.createElement('a');
//   document.body.appendChild(a);
//   a.style = 'display: none';
//   return (fileContent, fileName, fileMime) => {
//     const blob = new Blob([fileContent], { type: fileMime });
//     const url = window.URL.createObjectURL(blob);
//     a.href = url;
//     a.download = fileName;
//     a.click();
//     window.URL.revokeObjectURL(url);
//   };
// }());

function drawTriangle(ctx, size, trianglePos = { x: 0, y: 0 }) {
  const height = (Math.sqrt(3) * size) / 2;
  ctx.strokeStyle = 'black';
  ctx.lineWidth = 1;

  ctx.beginPath();
  ctx.moveTo(trianglePos.x + 0, trianglePos.y + 0);
  ctx.lineTo(trianglePos.x + size, trianglePos.y + 0);
  ctx.lineTo(trianglePos.x + size / 2, trianglePos.y + height);
  ctx.lineTo(trianglePos.x + 0, trianglePos.y + 0);
  ctx.stroke();
}

function getOptimalHoleDiameter(nbTrianglesBySide) {
  return triangleWidth / (nbTrianglesBySide - 1 + 3 / Math.sqrt(3));
}

function print(text) {
  document.getElementById('text').innerHTML += `<p>${text}<p>`;
}

function drawJarTriangle(ctx, jarTriangle, showConstruction = false, trianglePos = { x: 0, y: 0 }) {
  const optimalHoleDiameter = getOptimalHoleDiameter(jarTriangle.length);
  // magick number here, because I don't want to fix my calculations (see comments at the end of the page) for now.
  const interJars = ((optimalHoleDiameter - jarTriangle[0]) / 2) * 1.19;

  let coordX;
  let coordY;
  let pos;
  print(`triangle width: ${triangleWidth}`);
  print(`=== hole diameter: ${jarTriangle[0] + holePlay * 2} ===`);
  for (let nRow = 0; nRow < jarTriangle.length; nRow += 1) {
    print(`hole positions, line ${nRow + 1}:`);
    const d = jarTriangle[nRow] + holePlay * 2;
    const dWithGap = d + interJars;
    const dWithGaps = d + interJars * 2;
    const offset = { x: trianglePos.x + dWithGaps * (Math.sqrt(3) / 2), y: trianglePos.y + dWithGaps / 2 };
    for (let nCol = 0; nCol <= nRow; nCol += 1) {
      coordY = jarTriangle.length - nRow - 1;
      coordX = nCol + coordY * 0.5;
      pos = { x: offset.x + coordX * dWithGap, y: offset.y + coordY * dWithGap * (Math.sqrt(3) / 2) };
      if (showConstruction) {
        drawCircle(ctx, pos.x, pos.y, dWithGaps, true);
      }
      drawCircle(ctx, pos.x, pos.y, d);
      print(`${(pos.x - trianglePos.x).toFixed(1)}, ${(pos.y - trianglePos.y).toFixed(1)}`);
    }
  }
  // const d = jarTriangle[0] + holePlay * 2;
  // const optimaltriangleWidth = Math.sqrt(3) * (d + interJars * 2) + (jarTriangle.length - 1) * (d + interJars);
  drawTriangle(ctx, triangleWidth, trianglePos);
  return ctx;
}

function drawTriangles(showConstruction = false) {
  const ctx = new C2S(850, 750);
  let trianglePos;
  for (let i = 0; i < triangles.length; i += 1) {
    for (let j = 0; j < triangles[i].length; j += 1) {
      trianglePos = { x: i * triangleWidth, y: j * triangleWidth * (Math.sqrt(3) / 2) };
      drawJarTriangle(ctx, triangles[i][j], showConstruction, trianglePos);
    }
  }
  document.getElementById('drawing').innerHTML = ctx.getSerializedSvg(true);
}

function switchConstruction() {
  document.getElementById('show-constr').innerHTML = `${isConstr ? 'Show' : 'Hide'} construction`;
  isConstr = !isConstr;
  drawTriangles(isConstr);
}


drawTriangles(isConstr);
document.getElementById('show-constr').onclick = () => switchConstruction();
// document.getElementById('get-svg').onclick = () => saveData(svg, 'jar-wall.svg', 'image/svg+xml');

/*
I have triangles mapped in an equilateral triangle:

[![circles in triangle][1]][1]

The number of triangles can vary: 3, 6, 10, 15, etc.

I would like to calculate the circle diameter `d`, knowing the triangle width `a` and the number of circles by side `n` (here 1, 2 and 3, respectively).

Here is what I did already:

We calculate `a1`. [Wikipedia](https://en.wikipedia.org/wiki/Equilateral_triangle#Principal_properties) gives us the radius of the inscribed circle of an equilateral triangle, which is $d/2 = a1\frac{\sqrt{3}}{6}$, so:

$$a1 = \frac{\frac{d}{2}}{\frac{\sqrt{3}}{6}} = \frac{d}{2} \cdot \frac{6}{\sqrt{3}} = \frac{3 \cdot d}{\sqrt{3}} = d \cdot \frac{3}{\sqrt{3}}$$

Let `b` the distance between opposed circles centers of one side.

By construction, $b = d(n-1)$, and $b = a - a1$, so:

$$b = a - a1$$
$$d(n-1) = a - a1$$
$$d \cdot n-d = a - d \cdot \frac{3}{\sqrt{3}}$$
$$d \cdot n - d + d \cdot \frac{3}{\sqrt{3}} = a$$
$$d (n - 1 + \frac{3}{\sqrt{3}}) = a$$

$$d = \frac{a}{(n - 1 + \frac{3}{\sqrt{3}})}$$
*/
